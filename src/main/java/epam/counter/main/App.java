package epam.counter.main;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;


import epam.counter.impl.CounterForkJoin;
import epam.counter.impl.CounterThreadExecotor;
import epam.counter.impl.CounterWithoutThread;
/**
 * Hello world!
 *
 */
public class App {
	
	public static void main(String[] args) {

		List<File> roots =  Arrays.stream(File.listRoots()).filter(r -> r.exists()).collect(Collectors.toList());
		
		System.out.println("Count without multi-threading");
		long statrt = System.currentTimeMillis();
		CounterWithoutThread counterWithoutThread = new CounterWithoutThread(roots);
		counterWithoutThread.run();
		long end = System.currentTimeMillis();
		System.out.println("Time count without multi-threading "+(end - statrt));
		counterWithoutThread.show10popularName();
		
		System.out.println("\nCount whith for join");
		statrt = System.currentTimeMillis();
		CounterForkJoin counterForkJoin = new CounterForkJoin(roots);
		counterForkJoin.run();
		end = System.currentTimeMillis();
		System.out.println("Time count whith fork join "+(end - statrt));
		counterForkJoin.show10popularName();
		
		System.out.println("\nCount whith executor");
		statrt = System.currentTimeMillis();
		CounterThreadExecotor counterThreadExecotor = new CounterThreadExecotor(roots);
		counterThreadExecotor.run();
		end = System.currentTimeMillis();
		System.out.println("Time count whith executor "+(end - statrt));
		counterThreadExecotor.show10popular();
	}
}
