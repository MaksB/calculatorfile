package epam.counter.parser;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class Parser implements Serializable {

	public Map<TypeFile, List<String>> parsePatch(String path) {
		File file = new File(path);
		Map<TypeFile, List<String>> map = new EnumMap<>(TypeFile.class);
		map.put(TypeFile.FILE, new ArrayList<String>());
		map.put(TypeFile.FOLDER, new ArrayList<String>());
		if (Files.isReadable(file.toPath())) {
			List<String> items = Arrays.asList(file.list());
			if (!items.isEmpty()) {
				for (String item : items) {
					String newPath = path + "\\" + item;
					if (new File(newPath).isDirectory()) {
						map.get(TypeFile.FOLDER).add(newPath);
					} else {
						map.get(TypeFile.FILE).add(newPath);
					}
				}
			}
		}
		return map;
	}

}
