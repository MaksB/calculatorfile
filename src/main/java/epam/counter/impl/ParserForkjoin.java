package epam.counter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

import epam.counter.parser.Parser;
import epam.counter.parser.TypeFile;

public class ParserForkjoin extends RecursiveTask<List<String>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Parser parser;
	private String path;
	private List<String> result;

	public ParserForkjoin(String path) {
		this.path = path;
		this.parser = new Parser();
		this.result = new ArrayList<>();
	}

	@Override
	protected List<String> compute() {
		Map<TypeFile, List<String>> map = parser.parsePatch(path);
		for (Entry<TypeFile, List<String>> entry : map.entrySet()) {
			result.addAll(entry.getValue().stream().map(c -> c.replaceAll("^.*\\\\", "")).collect(Collectors.toList()));

		}

		if (!map.get(TypeFile.FOLDER).isEmpty()) {
			List<ParserForkjoin> subtask = new ArrayList<>();
			for (String newPath : map.get(TypeFile.FOLDER)) {
				subtask.add(new ParserForkjoin(newPath));
			}

			for (ParserForkjoin parserForkjoin : subtask) {
				parserForkjoin.fork();
			}
			
			for (ParserForkjoin parserForkjoin : subtask) {
				result.addAll(parserForkjoin.join());
			}
		}
		return result;
	}
}
