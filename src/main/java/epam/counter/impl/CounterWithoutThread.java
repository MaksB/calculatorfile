package epam.counter.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;

import epam.counter.parser.Parser;
import epam.counter.parser.TypeFile;

public class CounterWithoutThread {

	private List<File> roots;
	private List<String> result;
	private Parser parser;

	public CounterWithoutThread(List<File> roots) {
		this.roots = roots;
		this.result = new ArrayList<>();
		this.parser = new Parser();
	}

	public void run() {

		for (File file : roots) {
			save(parser.parsePatch(file.getPath()));
		}
		System.out.println("All count: " + result.size());
		
	}

	private void save(Map<TypeFile, List<String>> map) {
		for (Entry<TypeFile, List<String>> entry : map.entrySet()) {
			result.addAll(entry.getValue().stream().map(c -> c.replaceAll("^.*\\\\", "")).collect(Collectors.toList()));

		}

		if (!map.get(TypeFile.FOLDER).isEmpty()) {
			for (String path : map.get(TypeFile.FOLDER)) {
				save(parser.parsePatch(path));
			}
		}

	}

	public void show10popularName() {
		System.out.println("Distinct: " + getDistinct().size());
		System.out.println("Top 10:");
		Multiset<String> multiset = HashMultiset.create();
		multiset.addAll(result);
		Multisets.copyHighestCountFirst(multiset).stream().distinct().limit(10).forEach(System.out::println);
	}

	public List<String> getResult() {
		return result;
	}

	public void setResult(List<String> result) {
		this.result = result;
	}

	public Set<String> getDistinct() {
		return new HashSet<>(result);
	}

}
