package epam.counter.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;

public class CounterThreadExecotor {
	
	private List<File> roots;
	private List<String> result;

	public CounterThreadExecotor(List<File> roots) {
		this.roots = roots;
		this.result = new ArrayList<>();
	}
	
	public void run(){
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		for (File file : roots) {
			try {
				result.addAll(executorService.submit(new ParserExecutor(file.getPath())).get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		executorService.shutdown();
		System.out.println("All count: " + result.size());
	}
	
	public void show10popular(){
		System.out.println("Distinct: " + getDistinct().size());
		System.out.println("Top 10:");
		Multiset<String> multiset = HashMultiset.create();
		multiset.addAll(result);
		Multisets.copyHighestCountFirst(multiset).stream().distinct().limit(10).forEach(System.out::println);
	}

	public List<String> getResult() {
		return result;
	}

	public void setResult(List<String> result) {
		this.result = result;
	}
	
	public Set<String> getDistinct(){
		return new HashSet<>(result);
	}
	
	
}
