package epam.counter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import epam.counter.parser.Parser;
import epam.counter.parser.TypeFile;

public class ParserExecutor implements Callable<List<String>>{
	

	private Parser parser;
	private String path;
	private List<String> result;
	
	public ParserExecutor(String path) {
		this.path = path;
		this.parser = new Parser();
		this.result = new ArrayList<>();
	}
	
	@Override
	public List<String> call() throws Exception {

		Map<TypeFile, List<String>> map = parser.parsePatch(path);
		for (Entry<TypeFile, List<String>> entry : map.entrySet()) {
			result.addAll(entry.getValue().stream().map(c -> c.replaceAll("^.*\\\\", "")).collect(Collectors.toList()));

		}

		if (!map.get(TypeFile.FOLDER).isEmpty()) {	
			ExecutorService executorService = Executors.newFixedThreadPool(5);
			for (String newPath : map.get(TypeFile.FOLDER)) {
				result.addAll(executorService.submit(new ParserExecutor(newPath)).get());
			}
			executorService.shutdown();
		}
		return result;
	}
}
