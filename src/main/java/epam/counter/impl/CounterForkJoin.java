package epam.counter.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multisets;

public class CounterForkJoin {

	private List<File> roots;
	private List<String> result;

	public CounterForkJoin(List<File> roots) {
		this.roots = roots;
		this.result = new ArrayList<>();
	}

	public void run() {
		ForkJoinPool pool = new ForkJoinPool();
		for (File root : roots) {
			result.addAll(pool.invoke(new ParserForkjoin(root.getPath())));
		}
		pool.shutdown();
		System.out.println("All data: "+result.size());
	}

	public void show10popularName() {
	
		System.out.println("Discount: "+getDistinct().size());
		System.out.println("Top 10:");
		Multiset<String> multiset = HashMultiset.create();
		multiset.addAll(result);
		Multisets.copyHighestCountFirst(multiset).stream().distinct().limit(10).forEach(System.out::println);
	}

	public Set<String> getDistinct(){
		return new HashSet<>(result);
	}

}
